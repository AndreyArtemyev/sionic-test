## SIONIC

## Запуск девелоперского окружения

```shell script
docker-compose -f develop/docker-compose.yml -p sionic up -d
./develop/scripts/init_db.sh
sudo chmod -R 0777 ./www/assets
```

Удалить контейнеры:
```shell script
 ./develop/scripts/docker_rm_all.sh
 ```
 
inspect по-умолчанию:
```shell script
http://10.227.34.2     sionic-backend-web
http://10.227.34.3     sionic-backend-fpm
http://10.227.34.6     sionic-backend-pgadmin
http://10.227.34.4     sionic-backend-db
```

Адрес админки http://10.227.34.2/ :
```shell script
Login: admin
password: 123456
```

Адрес pgadmin http://10.227.34.6/ :
```shell script
Login: root
password: 123
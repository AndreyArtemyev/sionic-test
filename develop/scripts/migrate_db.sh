#!/bin/sh
# Запускает миграции Yii в контейнере

container_prefix="cito-backend"

docker exec $container_prefix"-fpm" bash -c "php protected/yiic migrate"

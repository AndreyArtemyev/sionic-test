#!/bin/sh
container_prefix="sionic-backend"

DB_COMMAND=$(cat <<-END
    mysql -hlocalhost -uroot -p123 -e "
    CREATE DATABASE IF NOT EXISTS sionic;
    use tomsk_ru;
    CREATE TABLE IF NOT EXISTS postfix_users (
      email varchar(255) NOT NULL,
      login varchar(255) NOT NULL,
      password varchar(255) NOT NULL,
      registered datetime NOT NULL,
      quota int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
END
)

docker exec $container_prefix"-mysql" sh -c "$DB_COMMAND";
